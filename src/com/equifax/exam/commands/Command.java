package com.equifax.exam.commands;

public enum Command {
	DEPEND, 
	INSTALL,
	REMOVE,
	LIST,
	END
}
