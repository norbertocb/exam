package com.equifax.exam.vo;

public class TcpIp {

	private String hostName;
	private String ipAddress;
	private String dhcpClient;
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getDhcpClient() {
		return dhcpClient;
	}
	public void setDhcpClient(String dhcpClient) {
		this.dhcpClient = dhcpClient;
	}
	@Override
	public String toString() {
		return "TcpIp [hostName=" + hostName + ", ipAddress=" + ipAddress + ", dhcpClient=" + dhcpClient + "]";
	}
}
