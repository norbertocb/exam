package com.equifax.exam.vo;

public class Netcard {
	private String name;
	private String ip;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	@Override
	public String toString() {
		return "Netcard [name=" + name + ", ip=" + ip + "]";
	}
	
}
