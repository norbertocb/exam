package com.equifax.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.equifax.exam.commands.Command;
import com.equifax.exam.vo.Browser;
import com.equifax.exam.vo.Html;
import com.equifax.exam.vo.Netcard;
import com.equifax.exam.vo.TcpIp;
import com.equifax.exam.vo.Telnet;

public class Bootsrap {

	public static void main(String[] args) throws IOException {
		System.out.println("Starting ProgA");
		Map<String, List<Object>> stackDependence = new HashMap<String, List<Object>>() ;
		Map<String, List<String>> commandAndParmas = null;
		Map<Object, List<Object> > stackCommand ;
//		final String[]  COMMANDS= {"DEPEND", "INSTALL","REMOVE", "LIST", "END"};
		boolean isRunning = true;
		
		do {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print(">");
		String inputConsole = br.readLine();
	
		try {
			commandAndParmas = validateInputCommand(inputConsole);
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		
		
		if (commandAndParmas != null) {
			String command = null;
			List<String> params = null;
			for ( Map.Entry<String, List<String>> entry : commandAndParmas.entrySet()) {
				command = entry.getKey();
				params = entry.getValue();
			}
			
			switch (command) {
			case "DEPEND" :
				System.out.println(command);
				try {
					List<Object>depends = dependCommand(params);
					stackDependence.put(Command.DEPEND.name(), depends);
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
				break;
			case "INSTALL" :
				System.out.println(command);
				break;
			case "REMOVE" :
				System.out.println(command);
				break;
			case "LIST" :
				System.out.println(command);
				break;
			case "END" :
				System.out.println(command);
				isRunning = false;
				break;
			}
		}
		
		}while(isRunning);
		
	}

	private static List<Object> dependCommand(List<String> params) throws Throwable {
		//Validate num mins of params
		if (params == null || params.size() <= 1)
			throw new Throwable("This command must have at least two parameter");
		
		List<Object> obj = new LinkedList<>();
		//get object for parms
		for (String nameParams : params) {
			
			if (nameParams.equalsIgnoreCase(Telnet.class.getSimpleName().toUpperCase()) ) {
				Telnet telnet = new Telnet();
				obj.add(telnet);
			}
			else if (nameParams.equalsIgnoreCase(TcpIp.class.getSimpleName().toUpperCase()) ) {
				TcpIp tcpIp = new TcpIp();
				obj.add(tcpIp);
			}
			else if (nameParams.equalsIgnoreCase(Netcard.class.getSimpleName().toUpperCase()) ) {
				Netcard netcard = new Netcard();
				obj.add(netcard);
			}
			else if (nameParams.equalsIgnoreCase(Html.class.getSimpleName().toUpperCase()) ) {
				Html netcard = new Html();
				obj.add(netcard);
			}
			else if (nameParams.equalsIgnoreCase(Browser.class.getSimpleName().toUpperCase()) ) {
				Browser netcard = new Browser();
				obj.add(netcard);
			}
			else {
				throw new Throwable("There is not a valid paramenter");
			}
		}
		return obj;
	}
	
	private static Map<String, List<String>> validateInputCommand(String fileLine) throws Throwable{
		String separateOperationsAndParams[] = fileLine.split("\\s+");
		List<String> paramsList = new ArrayList<String>();
		
		if (separateOperationsAndParams == null){
			throw new Throwable("There is not a command");
		}
		
		if (separateOperationsAndParams[0] == null || separateOperationsAndParams[0].trim().equalsIgnoreCase("")) {
			throw new Throwable("There is not a command");
		}
		
		String inputCommand = separateOperationsAndParams[0].trim();
		
		boolean isValidCommand = false;
		//validate if a text is a valid command
		for (Command command : Command.values()) {
			if (inputCommand.equalsIgnoreCase(command.name() )){
				paramsList = validateInputParams(separateOperationsAndParams);
				isValidCommand = true;
				break;
			}
		}
		Map<String, List<String> >commandAndParams = new HashMap<String,List<String>>();
		commandAndParams.put(inputCommand, paramsList);
		
		if (isValidCommand) {
			return commandAndParams;
		}else {
			new Throwable("There is not a valid command");
		}
		
		return commandAndParams;
	}
	
	private static List<String> validateInputParams(String[] separateOperationsAndParams){
		List<String> paramsList = new ArrayList<String>();
		
		for(int i=1; i<= separateOperationsAndParams.length -1; i++) {
			paramsList.add(separateOperationsAndParams[i]);
		}
		
		return paramsList;
	}
}
