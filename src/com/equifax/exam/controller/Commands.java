package com.equifax.exam.controller;

import java.util.List;

public interface Commands {
	
	public void depend(List<String> dependences);
	public void install ();
	public void remove();
	public void list();
	public void end();
	
	
}
